﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TccTeste.Telas
{
    public partial class CadastroProduto : UserControl
    {
        public CadastroProduto()
        {
            InitializeComponent();
        }

        private void CadastroProduto_Load(object sender, EventArgs e)
        {

        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {

            try
            {
                ProdutoClass.ProdutoDTO dto = new ProdutoClass.ProdutoDTO();
                dto.Produto = txtNome.Text;
                dto.Preco = Convert.ToDecimal(txtPreco.Text);

                ProdutoClass.ProdutoDatabase db = new ProdutoClass.ProdutoDatabase();
                db.Salvar(dto);

                MessageBox.Show("Produto salvo com sucesso!", "TccTeste", MessageBoxButtons.OK);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro: "+ ex.Message);
            }
        }
    }
}
