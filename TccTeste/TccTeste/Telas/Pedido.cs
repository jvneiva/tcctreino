﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TccTeste.PedidoClass;
using TccTeste.ProdutoClass;

namespace TccTeste.Telas
{
    public partial class Pedido : UserControl
    {
        public Pedido()
        {
            InitializeComponent();

            ProdutoBusiness business = new ProdutoBusiness();
            List<ProdutoDTO> lista = business.Listar();
            
            cboItem.ValueMember = nameof(ProdutoDTO.Id);
            cboItem.DisplayMember = nameof(ProdutoDTO.Produto);
            cboItem.DataSource = lista;
        }

          

        private void bnPedido_Click(object sender, EventArgs e)
        {
            PedidoDTO dto = new PedidoDTO();
            dto.Cliente = txtCliente.Text;
            dto.CPF = txtCPF.Text;
            dto.Data = dtpData.Value;
            
            PedidoDatabase db = new PedidoDatabase();
            db.Salvar(dto);

            MessageBox.Show("Pedido salvo com sucesso!", "TccTeste", MessageBoxButtons.OK);

            PedidoBusiness business = new PedidoBusiness();
            List<PedidoDTO> pedido = business.Listar();

            foreach (var item in pedido)
            {
                lstItem.Items.Add(item);
            }
            
        }

        private void lstItem_SelectedIndexChanged(object sender, EventArgs e)
        {
          
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
    }
}
