﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TccTeste.PedidoItemClass;
using TccTeste.ProdutoClass;

namespace TccTeste.PedidoClass
{
   public class PedidoBusiness
    {
        public int Salvar(PedidoDTO pedido, List<ProdutoDTO> produto)
        {
            PedidoDatabase db = new PedidoDatabase();
            int idPedido = db.Salvar(pedido);

            PedidoItemBusiness business = new PedidoItemBusiness();
            foreach (ProdutoDTO item in produto)
            {
                PedidoItemDTO dto = new PedidoItemDTO();
                dto.IdPedido = idPedido;
                dto.IdProduto = item.Id;

                business.Salvar(dto);
            }

            return idPedido;
        }

        public List<PedidoDTO> Listar()
        {
            PedidoDatabase db = new PedidoDatabase();
            List<PedidoDTO> lista = db.Listar();
            return lista;
        }
    }
}
