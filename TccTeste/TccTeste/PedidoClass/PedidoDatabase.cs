﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TccTeste.ClassesConnection;

namespace TccTeste.PedidoClass
{
    public class PedidoDatabase
    {
        public int Salvar(PedidoDTO dto)
        {
            string script = @"INSERT INTO tb_pedido(
             nm_cliente,
             ds_cpf,
             dt_venda) VALUES(
             @nm_cliente,
             @ds_cpf,
             @dt_venda)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", dto.Cliente));
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parms.Add(new MySqlParameter("dt_venda", dto.Data));


            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<PedidoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_pedido";

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<PedidoDTO> lista = new List<PedidoDTO>();

            while (reader.Read())
            {
                PedidoDTO dto = new PedidoDTO();
                dto.Id = reader.GetInt32("id_pedido");
                dto.Cliente = reader.GetString("nm_cliente");
                dto.CPF = reader.GetString("ds_cpf");
                dto.Data = reader.GetDateTime("dt_venda");

                lista.Add(dto);
            }
            reader.Close();
            return lista;

        }
    }
}
