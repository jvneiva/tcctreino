﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TccTeste.PedidoClass
{
   public class PedidoDTO
    {
        public int Id { get; set; }

        public string Cliente { get; set; }

        public string CPF { get; set; }

        public DateTime  Data { get; set; }
    }
}
