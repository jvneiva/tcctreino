﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace TccTeste.ClassesConnection
{
    class Connection
    {
        public MySqlConnection Create()
        {
            string connectionString = "server=localhost; database=TccTeste; uid=root; password=; SslMode=none";
            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();
            return connection;
        }
    }
}
