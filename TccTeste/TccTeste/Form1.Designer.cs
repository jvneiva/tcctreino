﻿namespace TccTeste
{
    partial class Form1
    {
        /// <summary>
        /// Variável de designer necessária.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpar os recursos que estão sendo usados.
        /// </summary>
        /// <param name="disposing">true se for necessário descartar os recursos gerenciados; caso contrário, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código gerado pelo Windows Form Designer

        /// <summary>
        /// Método necessário para suporte ao Designer - não modifique 
        /// o conteúdo deste método com o editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnVenda = new System.Windows.Forms.Button();
            this.btnCP = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pnlCentro = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.pnlCentro.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnVenda
            // 
            this.btnVenda.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnVenda.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnVenda.Location = new System.Drawing.Point(12, 186);
            this.btnVenda.Name = "btnVenda";
            this.btnVenda.Size = new System.Drawing.Size(110, 38);
            this.btnVenda.TabIndex = 0;
            this.btnVenda.Text = "Pedido";
            this.btnVenda.UseVisualStyleBackColor = false;
            this.btnVenda.Click += new System.EventHandler(this.btnVenda_Click);
            // 
            // btnCP
            // 
            this.btnCP.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnCP.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnCP.Location = new System.Drawing.Point(12, 126);
            this.btnCP.Name = "btnCP";
            this.btnCP.Size = new System.Drawing.Size(110, 39);
            this.btnCP.TabIndex = 1;
            this.btnCP.Text = "Cadastrar Produto";
            this.btnCP.UseVisualStyleBackColor = false;
            this.btnCP.Click += new System.EventHandler(this.btnCP_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.LightGray;
            this.panel1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.panel1.Controls.Add(this.btnCP);
            this.panel1.Controls.Add(this.btnVenda);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(133, 363);
            this.panel1.TabIndex = 2;
            // 
            // pnlCentro
            // 
            this.pnlCentro.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.pnlCentro.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pnlCentro.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlCentro.Controls.Add(this.label1);
            this.pnlCentro.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCentro.Location = new System.Drawing.Point(133, 0);
            this.pnlCentro.Name = "pnlCentro";
            this.pnlCentro.Size = new System.Drawing.Size(370, 363);
            this.pnlCentro.TabIndex = 3;
            this.pnlCentro.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(53, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(259, 86);
            this.label1.TabIndex = 0;
            this.label1.Text = "JvStore";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
            this.ClientSize = new System.Drawing.Size(503, 363);
            this.Controls.Add(this.pnlCentro);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TccTeste";
            this.panel1.ResumeLayout(false);
            this.pnlCentro.ResumeLayout(false);
            this.pnlCentro.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnVenda;
        private System.Windows.Forms.Button btnCP;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnlCentro;
        private System.Windows.Forms.Label label1;
    }
}

