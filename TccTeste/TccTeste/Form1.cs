﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TccTeste.Telas;

namespace TccTeste
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public void OpenScreen(UserControl control)
        {
            if (pnlCentro.Controls.Count == 1)
                pnlCentro.Controls.RemoveAt(0);
            pnlCentro.Controls.Add(control);
        }

        private void btnVenda_Click(object sender, EventArgs e)
        {
            Pedido tela = new Pedido();
            OpenScreen(tela);
        }

        private void btnCP_Click(object sender, EventArgs e)
        {
            CadastroProduto tela = new CadastroProduto();
            OpenScreen(tela);
            
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
