﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TccTeste.PedidoItemClass
{
    public class PedidoItemBusiness
    {
        public int Salvar(PedidoItemDTO dto)
        {
            PedidoItemDatabase db = new PedidoItemDatabase();
            return db.Salvar(dto);
        }

        public List<PedidoItemDTO> Listar()
        {
            PedidoItemDatabase db = new PedidoItemDatabase();
            List<PedidoItemDTO> lista = db.Listar();
            return lista;
        }
    }
}
