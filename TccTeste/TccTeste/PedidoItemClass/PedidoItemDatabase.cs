﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TccTeste.ClassesConnection;

namespace TccTeste.PedidoItemClass
{
    public class PedidoItemDatabase
    {
        public int Salvar(PedidoItemDTO dto)
        {
            string script = @"INSERT INTO tb_pedido_item(
            id_pedido,
            id_produto) VALUES(
            @id_pedido,
            @id_produto)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido", dto.IdPedido));
            parms.Add(new MySqlParameter("id_produto", dto.IdProduto));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<PedidoItemDTO> Listar()
        {
            string script = @"SELECT * FROM tb_pedido_item";

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<PedidoItemDTO> lista = new List<PedidoItemDTO>();

            while (reader.Read())
            {
                PedidoItemDTO dto = new PedidoItemDTO();
                dto.Id = reader.GetInt32("id_pedido_item");
                dto.IdPedido = reader.GetInt32("id_pedido");
                dto.IdProduto = reader.GetInt32("id_produto");


                lista.Add(dto);
            }
            reader.Close();
            return lista;
        }
    }
}
