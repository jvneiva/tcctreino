﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TccTeste.ProdutoClass
{
    public class ProdutoDTO
    {
        public int Id { get; set; }

        public string Produto { get; set; }

        public decimal Preco { get; set; }
    }
}
