﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TccTeste.ProdutoClass
{
    public class ProdutoBusiness
    {
        public int Salvar(ProdutoDTO dto)
        {
            ProdutoDatabase db = new ProdutoDatabase();

            if (dto.Produto == string.Empty)
            {
                throw new Exception("O campo 'Nome' não pode estar em branco.");
            }

            if (dto.Preco == null)
            {
                throw new Exception("O campo 'Preço' não pode estar em branco.");
            }

            int id = db.Salvar(dto);
            return id;
        }

        public List<ProdutoDTO> Listar()
        {
            ProdutoDatabase db = new ProdutoDatabase();
            List<ProdutoDTO> lista = db.Listar();
            return lista;
        }
    }
}
