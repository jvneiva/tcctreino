﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TccTeste.ClassesConnection;

namespace TccTeste.ProdutoClass
{
    public class ProdutoDatabase
    {
        public int Salvar(ProdutoDTO dto)
        {
            string script = @"INSERT INTO tb_produto(
            nm_produto,
            vl_preco) VALUES(
            @nm_produto,
            @vl_preco)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_produto", dto.Produto));
            parms.Add(new MySqlParameter("vl_preco", dto.Preco));

            Database db = new Database();
            int pk = db.ExecuteInsertScriptWithPk(script, parms);
            return pk;
        }

        public List<ProdutoDTO> Listar()
        {
            string script = @"SELECT * FROM tb_produto";

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, null);

            List<ProdutoDTO> lista = new List<ProdutoDTO>();

            while (reader.Read())
            {
                ProdutoDTO dto = new ProdutoDTO();
                dto.Id = reader.GetInt32("id_produto");
                dto.Produto = reader.GetString("nm_produto");
                dto.Preco = reader.GetDecimal("vl_preco");

                lista.Add(dto);
            }
            reader.Close();
            return lista;

        }
    }
}
